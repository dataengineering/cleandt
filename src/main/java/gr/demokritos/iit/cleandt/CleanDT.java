/**
 * 
 */
package gr.demokritos.iit.cleandt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Giannis Mouchakis
 *
 */
public class CleanDT {
	
	
	public CleanDT() {
		super();
	}
	

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws IOException, ParseException {
		
		CleanDT cleanDT = new CleanDT();
		
        InputStream in = cleanDT.getClass().getClassLoader().getResourceAsStream("agris_original_dates");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        
        Path path_mappings = Paths.get(System.getProperty("user.home") + File.separator + "mappings");
        BufferedWriter mappings = Files.newBufferedWriter(path_mappings, StandardCharsets.UTF_8);
        
        Path path_unmapped = Paths.get(System.getProperty("user.home") + File.separator + "unmapped");
        BufferedWriter unmapped = Files.newBufferedWriter(path_unmapped, StandardCharsets.UTF_8);
        
        int transformed = 0;
        int not_transformed = 0;
        
        String line = null;
       
        while ( (line = reader.readLine()) != null ) {

			String transformed_date = cleanDT.format(line);
			if (transformed_date != null) {
				mappings.write(line);
				mappings.write(", ");
				mappings.write(transformed_date);
				mappings.newLine();
				transformed++;
			} else {
				unmapped.write(line);
				unmapped.newLine();
				not_transformed++;
			}
            
        }
        
        mappings.close();
        unmapped.close();
        
        System.out.println("mapped=" + transformed);
        System.out.println("unmapped=" + not_transformed);
	}
	
	public String format(String original_date) throws IOException, ParseException {
		
		String tranformed_date = null;
		
		if (original_date.matches("\\d{4}")
				|| original_date.matches("\\d{4}(-|/)\\d{1,2}")
				|| original_date.matches("\\d{4}(-|/)\\d{1,2}(-|/)\\d{1,2}")) {
			tranformed_date = original_date.replaceAll("/", "-");
    	} else if ( original_date.matches("\\d{1,2}(-|/)\\d{4}")) {//MM-yyyy
			original_date = original_date.replaceAll("/", "-");
			Date date = new SimpleDateFormat("MM-yyyy").parse(original_date);
			SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM");
			tranformed_date = date_format.format(date);
    	} else if ( original_date.matches("\\d{1,2}(-|/)\\d{1,2}(-|/)\\d{4}")) {//dd-MM-yyyy
			original_date = original_date.replaceAll("/", "-");
			Date date = new SimpleDateFormat("dd-MM-yyyy").parse(original_date);
			SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
			tranformed_date = date_format.format(date);
    	} else if ( original_date.matches("\\d{8}") ){
			Date date = new SimpleDateFormat("yyyyMMdd").parse(original_date);
			SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
			tranformed_date = date_format.format(date);
    	} else if (original_date.matches("win\\d{4}")) {
			original_date = original_date.replaceAll("win", "");
			tranformed_date = original_date + "-01";
    	}  else if (original_date.matches("sum\\d{4}")) {
			original_date = original_date.replaceAll("sum", "");
			tranformed_date = original_date + "-06";
    	}  else if (original_date.matches("aut\\d{4}")) {
			original_date = original_date.replaceAll("aut", "");
			tranformed_date = original_date + "-09";
    	}  else if (original_date.matches("spr\\d{4}")) {
			original_date = original_date.replaceAll("spr", "");
			tranformed_date = original_date + "-03";
		} else if (original_date.matches("\\d{1,2}\\D{3}-\\d{1,2}\\D{3}\\d{4}")
				&& formatddMMMyyyy(original_date.replaceAll("-\\d{1,2}\\D{3}", "")) != null) {// eg 25apr-1may1998 
			tranformed_date = formatddMMMyyyy(original_date.replaceAll("-\\d{1,2}\\D{3}", ""));
    	} else if (formatddMMMyyyy(original_date) != null) {
    		tranformed_date = formatddMMMyyyy(original_date);
    	} else if (formatMMMyyyy(original_date) != null) {
    		tranformed_date = formatMMMyyyy(original_date);
    	} else if (original_date.matches("\\d{1,2}(-|/)\\d{1,2}\\D{3}\\d{4}")) {
    		original_date = original_date.replaceAll("/", "-");
    		original_date = original_date.substring(original_date.indexOf("-") + 1);
			if (formatddMMMyyyy(original_date) != null) {
				tranformed_date = formatddMMMyyyy(original_date);
			}
		}
		
    	return tranformed_date;

	}
	
	public String formatddMMMyyyy(String original_date) throws IOException {
		SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
		String tranformed_date = null;
		try {
			Date date = new SimpleDateFormat("ddMMMyyyy").parse(original_date);
			if (original_date.length()<=9) {
				tranformed_date = date_format.format((date));
			}
		} catch (ParseException e1) {
			//do spanish
			try {
				Date date = new SimpleDateFormat("ddMMMyyyy", new Locale("es", "ES")).parse(original_date);
				if (original_date.length()<=9) {
					tranformed_date = date_format.format((date));
				}
			}  catch (ParseException e2) {
				//do italian
				try {
					Date date = new SimpleDateFormat("ddMMMyyyy", Locale.ITALY).parse(original_date);
					if (original_date.length()<=9) {
						tranformed_date = date_format.format((date));
					}
				} catch (ParseException e3) {
					//do french
					try {
						Date date = new SimpleDateFormat("ddMMMyyyy", Locale.FRANCE).parse(original_date);
						if (original_date.length()<=9) {
							tranformed_date = date_format.format((date));
						}
					} catch (ParseException e4) {
						//do german
						try {
							Date date = new SimpleDateFormat("ddMMMyyyy", Locale.GERMANY).parse(original_date);
							if (original_date.length()<=9) {
								if (original_date.length()<=9) {
									tranformed_date = date_format.format((date));
								}
							}
						} catch (ParseException e5) {
							tranformed_date = null;
						}
					}
				}
			}
		}
		return tranformed_date;
	}
	
	public String formatMMMyyyy(String original_date) throws IOException {
		SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM");
		String tranformed_date = null;
		try {
			Date date = new SimpleDateFormat("MMMyyyy").parse(original_date);
			if (original_date.length()<=7) {
				tranformed_date = date_format.format((date));
			}
		} catch (ParseException e1) {
			//do spanish
			try {
				Date date = new SimpleDateFormat("MMMyyyy", new Locale("es", "ES")).parse(original_date);
				if (original_date.length()<=7) {
					if (original_date.length()<=7) {
						tranformed_date = date_format.format((date));
					}
				}
			} catch (ParseException e2) {
				//do italian
				try {
					Date date = new SimpleDateFormat("MMMyyyy", Locale.ITALY).parse(original_date);
					if (original_date.length()<=7) {
						if (original_date.length()<=7) {
							tranformed_date = date_format.format((date));
						}
					}
				} catch (ParseException e3) {
					//do french
					try {
						Date date = new SimpleDateFormat("MMMyyyy", Locale.FRANCE).parse(original_date);
						if (original_date.length()<=7) {
							if (original_date.length()<=7) {
								tranformed_date = date_format.format((date));
							}
						}
					} catch (ParseException e4) {
						//do german
						try {
							Date date = new SimpleDateFormat("MMMyyyy", Locale.GERMANY).parse(original_date);
							if (original_date.length()<=7) {
								if (original_date.length()<=7) {
									tranformed_date = date_format.format((date));
								}
							}
						} catch (ParseException e5) {
							tranformed_date = null;
						}
					}
				}
			}
		}
		return tranformed_date;

	}

}
